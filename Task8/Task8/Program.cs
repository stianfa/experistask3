﻿using System;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {

            // Collect and parse size of square from the user
            Console.WriteLine("Please enter size of the square:");
            int size = Convert.ToInt32(Console.ReadLine());

            //Call the method that draws the square
            DrawSquare(size);

        }


        /// <summary>
        /// Draws a square according to the size given
        /// </summary>
        /// <param name="size">Length of one side of the square</param>
        /// 
        public static void DrawSquare(int size)
        {
            // Iterate through the coordinates and draw  the figure
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i == 0 || i == size - 1 || j == 0 || j == size - 1)
                    {
                        Console.Write("#");                                   // Draw the outer border
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }
    }
}
