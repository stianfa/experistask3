﻿using System;
using System.Collections.Generic;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {

            /* Example square (4x4):
                    ####
                    #  #
                    #  #
                    ####
            */

            // Collect and parse size of square from the user
            Console.WriteLine("Please enter size of the square:");
            int size = Convert.ToInt32(Console.ReadLine());

            // Iterate through the coordinates and draw  the figure
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if(i == 0 || i == size-1 || j == 0 || j == size-1)
                    {
                        Console.Write("#");                                 // Draw the outer border
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
